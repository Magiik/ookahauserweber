import Command.*;

import java.io.IOException;
import java.util.Scanner;

import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;
import LZU.*;

public class MainUebung3 {

    public static void main(String[] args) throws MultipleStopMethodsException, MultipleStartMethodsException, IOException, ClassNotFoundException {
                
        /*
        Commands:
        LZU
            -startLZU --> Starten der LZU
            -stopLZU --> Stoppen der LZU

            #List of loaded Components
            -importComponent(url) --> Hinzufügen der Klassen in die alt.LZU // prüfen ob einmalig geladen.
            -removeComponent(id) --> Entfernen der Klassen aus der alt.LZU
            -listImports --> gibt Liste der geladenen Komponenten zurück

            -listComponents --> Listet alle Intanzen von Klassen // Status der Comp speichern in Liste.
            -createComponent --> Erstellt eine Instanz einer importierten Komponente
            -startComponent [list:id] --> Startet eine Instanz //Name evtl Name des Package oder selbst vergeben.
            -stopComponent [list:id] --> Beendet eine Instanz
        */

        /*System.out.println("alt.LZU mit \"alt.LZU start\" starten");
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            String line = sc.nextLine();
            Scanner tmpScanner = new Scanner(line);

            sc.nextLine();
        }*/

        /*LZU.startLZU();
        try {
            LZU.addComponent("C:\\Users\\Tobi\\Nextcloud2\\Studium Tobi\\Master Informatik\\2. Semester\\OOKA\\Übung 1\\OOKACode\\OOKA\\Ü1\\code\\target\\codesOOKA-1.0-SNAPSHOT.jar");
        } catch (MalformedURLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        LZU.stopLZU();
        LZU.startLZU();*/



        /*
        try {
            URL url = new URL("file:C:\\Users\\Tobi\\Desktop\\codesOOKA-1.0-SNAPSHOT.jar");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        LZU.getInstance().addComponent("C:\\Users\\Tobi\\Desktop\\codesOOKA-1.0-SNAPSHOT.jar");
        HashMap<Integer, String> addedComps = LZU.getInstance().listImportedComponents();
        System.out.println(addedComps.toString());

        LZU.getInstance().createComponent(0);
        System.out.println((LZU.getInstance().listComponents().toString()));
        */

        /*LZUCommandExecuter executer = new LZUCommandExecuter();
        StartLZUCommand start = new StartLZUCommand();
        StopLZUCommand stop = new StopLZUCommand();
        executer.executeCommand(start);
        executer.executeCommand(stop);*/
        //LZU.getInstance().addComponent("file:C:\\Users\\Tobi\\Desktop\\codesOOKA-1.0-SNAPSHOT.jar");
        //LZU.getInstance().addComponent("file:C:\\Users\\Tobi\\Documents\\.Master Informatik\\OOKA\\target\\OOKA-1.0-SNAPSHOT.jar");
        LZU.getInstance().addComponent("file:Uebung2/hbrs/bonn/testJars/simpleTest.jar");
        LZU.getInstance().createComponent(0);
        LZU.getInstance().createComponent(0);
        System.out.println(LZU.getInstance().listComponents());
        LZU.getInstance().startComponent(0);
        System.out.println(LZU.getInstance().listComponents());
        LZU.getInstance().stopComponent(0);
        System.out.println(LZU.getInstance().listComponents());
    }
    
    private static void startCLI () {
        System.out.println("CLI gestartet!");
        Scanner scanner = new Scanner(System.in);
        LZUCommandExecuter commandExecuter = new LZUCommandExecuter();
        System.out.print("> ");
        
        while (scanner.hasNextLine()) {
            LZUOperation command = null;
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            boolean skip = false;

            while (lineScanner.hasNext() && !skip) {
                switch (lineScanner.next().toLowerCase()) {
                    case "startlzu":
                        command = new StartLZUCommand();
                        break;
                    case "stoplzu":
                        command = new StopLZUCommand();
                        break;
                    case "importcomponent":
                        if (lineScanner.hasNext()) {
                            String url = "file:";
                            while (lineScanner.hasNext()) {
                                url += lineScanner.next();
                            }
                            command = new ImportComponentToLZUCommand(url);
                        } else {
                            System.out.println("Nutzung: importComponent [url]");
                        }
                        break;
                    case "removecomponent":
                        try {
                            command = new RemoveComponentFromLZUCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "listimports":
                        command = new ListImportsCommand();
                        break;
                    case "listcomponents":
                        command = new ListComponentsCommand();
                        break;
                    case "createcomponent":
                        try {
                            command = new CreateComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "startcomponent":
                        try {
                            command = new StartComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "stopcomponent":
                        try {
                            command = new StopComponentCommand(Integer.parseInt(lineScanner.next()));
                        } catch (NumberFormatException e) {
                            System.out.println("Der angegebene Parameter ist keine Zahl!");
                        }
                        break;
                    case "state":
                        System.out.println(LZU.getInstance().getState(Integer.parseInt(lineScanner.next())));
                        break;
                    default:
                        System.out.println("Kommando nicht gefunden!");
                        skip = true;
                        break;
                }
            }

            if (command != null) {
                commandExecuter.executeCommand(command);
            }

            System.out.print("> ");
        }
    }
}