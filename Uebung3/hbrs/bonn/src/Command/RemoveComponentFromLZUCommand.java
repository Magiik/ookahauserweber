package Command;

import LZU.LZU;

public class RemoveComponentFromLZUCommand implements LZUOperation{

    private int id;

    public RemoveComponentFromLZUCommand (int id) {
        this.id = id;
    }


    @Override
    public void execute() {
        LZU.getInstance().removeComponent(id);
    }
}
