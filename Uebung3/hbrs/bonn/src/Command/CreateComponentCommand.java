package Command;

public class CreateComponentCommand implements LZUOperation {

    private int id;

    public CreateComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        LZU.LZU.getInstance().createComponent(this.id);
    }
}
