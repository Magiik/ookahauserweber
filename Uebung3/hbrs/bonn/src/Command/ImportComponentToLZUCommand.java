package Command;

import LZU.LZU;

import java.net.MalformedURLException;

public class ImportComponentToLZUCommand implements LZUOperation{

    private String url;

    public ImportComponentToLZUCommand(String url) {
        this.url = url;
    }

    @Override
    public void execute() {
        LZU.getInstance().addComponent(url);
    }
}
