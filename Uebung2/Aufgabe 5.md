Die implementierte Lösung erfüllt nach Crnkovic hinsichtlich seines definierten Lifecycles die Trennung zwischen [Requierements, Design, Implementation, Packaging] und [Deployment, Execution]. 
Insbesondere wurde hier das Packaging über die JAR Files (Komponenten) abgebildet, sowie das Deployment mit dem Laden der JAR Files zur Laufzeit (Runtime).
Die LZU übernimmt die Execution der Kompnenten nach Crnkovic.

Die Construction Kriterien der Komponente Buchungssystem wurden mithilfe von Interfaces, welche die Komponente bereitstellt, in der letzten Übung gelöst (provided Interfaces). 
Wir denken die Ports stellen hier die Horizontale Bindung zwischen den Komponenten in Form vom „Glue Code“ dar. 
Da die Verbindung mithilfe von Ports (connectors) implementiert ist, gehen wir von einer exogenous composition nach Crnkovic aus.
Die Kommunikation zwischen den Kompónenten ist nicht als asyncron oder syncron definiert.
