package Command;

public class StopComponentCommand implements LZUOperation {

    private int id;

    public StopComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        LZU.LZU.getInstance().stopComponent(id);
    }
}
