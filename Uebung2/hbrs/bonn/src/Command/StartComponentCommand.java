package Command;

public class StartComponentCommand implements LZUOperation {

    private int id;

    public StartComponentCommand (int id) {
        this.id = id;
    }

    @Override
    public void execute() {
        LZU.LZU.getInstance().startComponent(this.id);
    }
}
