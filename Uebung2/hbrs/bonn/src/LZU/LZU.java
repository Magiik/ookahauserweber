package LZU;

import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LZU {
    private List<String> componentURLs;
    private List<Component> components;
    private static LZU lzu;

    private LZU() {

    }

    public void stopLZU() {
        lzu = null;
        System.out.println("LZU gestoppt");
    }

    public static LZU getInstance() {
        if (lzu == null) {
            System.out.println("Starte LZU");
            lzu = new LZU();
            System.out.println("LZU gestartet");
        }

        return lzu;
    }

    public void addComponent(String url) {
        // Prüfen, ob die URL korrekt geformt ist
        try {
            URL u = new URL(url);
        } catch (MalformedURLException e) {
            System.out.println("Der angegebene Parameter ist keine URL!");
            return;
        }

        if (componentURLs == null) {
            componentURLs = new ArrayList<>();
        }

        if (componentURLs.contains(url)) {
            System.out.println("Komponente bereits hinzugefügt");
            return;
        }

        url = url.substring(5);

        componentURLs.add(url);
        System.out.println("Komponente hinzugefügt");
    }

    // TODO: Alle dazugehörigen Instanzen der Komponenten beenden. Dazu muss eine Zuordnung zu den laufenden Komponenten existieren.
    public void removeComponent(int id) {
        try {
            componentURLs.remove(id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste enthalten!");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten hinzugefügt.");
        }
    }

    // TODO: Die Liste "componentURLs" ausgeben
    public HashMap<Integer, String> listImportedComponents() {
        HashMap<Integer, String> output = new HashMap<>();
        int i = 0;

        for (String c : componentURLs) {
            output.put(i, c);
        }

        return output;
    }

    public void createComponent(int id) {
        String url;
        try {
            url = componentURLs.get(id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("ID nicht in der Liste der Komponenten.");
            return;
        }

        if (components == null) {
            components = new ArrayList<>();
        }

        try {
            Component newComponent = new Component(url);
            components.add(newComponent);
        } catch (MalformedURLException | ClassNotFoundException | MultipleStartMethodsException | MultipleStopMethodsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("URL wurde nicht gefunden!");
            e.printStackTrace();
        }
    }

    public synchronized void startComponent (int componentID) {
        try {
            components.get(componentID).start();
            System.out.println(components.get(componentID).getState());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste vorhanden.");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten erstellt.");
        }
    }

    public synchronized void stopComponent (int componentID) {
        try {
            components.get(componentID).stopInit();
            components.get(componentID).interrupt();
            System.out.println(components.get(componentID).getState());
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Die angegebene ID ist nicht in der Liste vorhanden.");
        } catch (NullPointerException e) {
            System.out.println("Es wurden noch keine Komponenten erstellt.");
        }
    }

    // TODO: Hier sollten nur die Komponenten ausgegeben werden, die in der Liste "components" sind. Dazu den Status der Komponente ausgeben.
    public synchronized List<String> listComponents() {
        List<String> output = new ArrayList<>();

        if (components == null) {
            System.out.println("Es wurden noch keine Komponenten erstellt!");
            return output;
        }

        int id = 0;
        output.add("ID  |   Name    |   Status");
        for (Component c : components) {
            String idString = Integer.toString(id);
            String name = c.getName();
            String status = c.getStatus();
            output.add(idString + "   |   " + name + "    |   " + status + "\n");
        }

        return output;
    }

    public synchronized String getState(int id) {
        return String.valueOf(components.get(id).getState());
    }
}
