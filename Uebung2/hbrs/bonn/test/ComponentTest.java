import Exceptions.MultipleStartMethodsException;
import Exceptions.MultipleStopMethodsException;
import LZU.Component;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Collection;

public class ComponentTest {
    @BeforeAll
    static void setup () {
        System.out.println("Starte Tests");
    }

    @Test
    void loadIdenticalComponentsIntoSameClassLoader() {
        String url = "Uebung2/hbrs/bonn/testJars/simpleTest.jar";
        Component c1 = new Component();

        try {
            c1.setClassLoader(url);
            c1.loadClassFromJAR(url);
            Collection<Class<?>> classes1 = c1.getClasses().values();

            c1.loadClassFromJAR(url);
            Collection<Class<?>> classes2 = c1.getClasses().values();

            Assertions.assertEquals(classes1.size(), classes2.size());

            for (int i = 0;i < classes1.size();++i) {
                Class<?> clazz1 = (Class<?>) classes1.toArray()[i];
                Class<?> clazz2 = (Class<?>) classes2.toArray()[i];

                Assertions.assertEquals(clazz1.getClassLoader(), clazz2.getClassLoader());
                Assertions.assertEquals(clazz1.getName(), clazz2.getName());
                Assertions.assertEquals(clazz1, clazz2);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void findStartMethodAndClassByURL() {
        String url = "Uebung2/hbrs/bonn/testJars/simpleTest.jar";
        Component component = new Component();
        try {
            component.setClassLoader(url);
            component.loadClassFromJAR(url);
            component.findStartMethod();
            Method start = component.getStartMethod();
            Class<?> startClass = component.getStartClass();

            Assertions.assertNotNull(start);
            Assertions.assertNotNull(startClass);
            Assertions.assertEquals(start.getName(), "start");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (MultipleStartMethodsException e) {
            e.printStackTrace();
        }
    }

    @Test
    void twoStartMethodsInComponent () {
        String url = "Uebung2/hbrs/bonn/testJars/componentWithTwoStartAnnotations.jar";
        Component component = new Component();
        try {
            component.setClassLoader(url);
            component.loadClassFromJAR(url);
            Assertions.assertThrows(MultipleStartMethodsException.class, component::findStartMethod);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    void startComponentInThreadAndInterruptIt() {
        String url = "Uebung2/hbrs/bonn/testJars/simpleTest.jar";
        try {
            Component c = new Component(url);
            Assertions.assertEquals(c.getState().toString(), "NEW");
            c.start();
            System.out.println(c.getState().toString());
            Assertions.assertEquals(c.getState().toString(), "RUNNABLE");
        } catch (MultipleStopMethodsException e) {
            e.printStackTrace();
        } catch (MultipleStartMethodsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
